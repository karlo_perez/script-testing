<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-compatible" content="IE-Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, noarchive">

    <title>Script Testing</title>

    <!-- EXTERNAL STYLING -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        .container {
            margin: 20px;
            padding: 20px;
            border: 1px solid #aaa;
            border-radius: 6px;
        }
        li {
            margin: 6px 0;
        }
    </style>
</head>
<body>

    <script>
        function toCelsius( f ) {
            return ( 5/9 ) * ( f-32 );
        }

        function deleteRow( deleteItem ) {
            listItem = deleteItem.parentNode;
            listItem.parentNode.removeChild( listItem );
        }

        function deleteTableRow( deleteRow ) {
            tableRow = deleteRow.parentNode.parentNode; // should have been the same as deleteRow()
            tableRow.parentNode.removeChild( tableRow );

            basketListRows = basketList.rows.length;
            console.log( basketListRows );
        }

        function calculateTotalPrice( totalRowPrice ) {
            for ( i = 0 ; i < totalRowPrice.length ; i++ ) {
                totalPrice += parseInt( totalRowPrice[i].innerHTML );
                console.log( totalPrice );
            }
        }
    </script>
    

    <div class="container">
        <h2>JavaScript Functions</h2>
        <p>This example converts Fahrenheit to Celsius:</p>

        <input type="text" id="inputTemperature">
        <input type="button" class="btn btn-primary" id="submitTemperature" value="Submit Temperature">

        <p id="outputTemperature"></p>

        <script>
            document.getElementById( 'submitTemperature' ).addEventListener( 'click', function() {
                x = document.getElementById( 'inputTemperature' ).value;
                x = toCelsius(x);

                document.getElementById( 'outputTemperature' ).innerHTML = x + '°C';
            } );
        </script>
    </div>  
    

    <div class="container">
        <h2>To-do List</h2>
        <p>This example adds and removes items from a To-do list:</p>

        <input type="text" id="inputNotes">
        <input type="button" class="btn btn-primary" id="submitNotes" value="Submit Notes">

        <ol id="outputNotes"></ol>

        <script>
            document.getElementById( 'submitNotes' ).addEventListener( 'click', function() {
                x = document.getElementById( 'inputNotes' ).value;

                if ( x == '' ) {
                    console.log( 'Field is blank' ); 

                    alert( 'Field is blank' );
                } else {
                    document.getElementById( 'outputNotes' ).innerHTML +=
                        '<li>'+ x + ' <input type="button" class="btn btn-danger" onclick="deleteRow(this)" value="Remove Note"> </li>';
                }
            } );
        </script>
    </div>


    <div class="container">
        <h2>Discounting per Item Ordered</h2>
        <p>This example enlists items unto Basket and allows Discounting of each item</p>
            
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" id="inputItemName"></td>
                    <td><input type="number" id="inputItemPrice"></td>
                    <td><input type="number" id="inputItemQuantity"></td>
                </tr>
                <tr>
                    <td><input type="button" id="submitItem" value="Create Item"></td>
                </tr>
            </tbody>
        </table>
        
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Item Name</th>
                    <th scope="col">Item Price</th>
                    <th scope="col">Item Quantity</th>
                    <th scope="col">Item Discount</th>
                    <th scope="col">Item Price</th>
                </tr>
            </thead>
            <tbody id="basket">
                <!-- items are inserted here as rows -->
                <tr>
                    <td colspan="4">Total Price</td>
                    <td id="totalPrice"></td>
                </tr>
            </tbody>
        </table>

        <script>
            document.getElementById( 'submitItem' ).addEventListener( 'click', function() {
                basketList = document.getElementById( 'basket' );

                outputItemName = document.getElementById( 'inputItemName' ).value;
                outputItemPrice = document.getElementById( 'inputItemPrice' ).value;
                outputItemQuantity = document.getElementById( 'inputItemQuantity' ).value;
                outputRowPrice = outputItemPrice * outputItemQuantity;
                  

                if ( outputItemName != '' ) {
                    if ( outputItemPrice != '' ) {
                        if ( outputItemQuantity != '' ) {

                            basketList.innerHTML +=
                                '<tr> <td>' + outputItemName + '</td> <td>' + outputItemPrice + '</td> <td>' + outputItemQuantity + '</td> <td> <input type="number" class="discount"> </td> <td class="output-row-price">' + outputRowPrice + '</td> <td> <input type="button" class="btn btn-danger" onclick="deleteTableRow(this)" value="-"> </td> </tr>';
                            basketListRows = basketList.rows.length;
                           
                            if ( basketListRows > 0 ) {
                                totalPrice = 0;
                                totalRowPrice = document.getElementsByClassName( 'output-row-price' );
    
                                calculateTotalPrice( totalRowPrice );
                                
                                document.getElementById( 'totalPrice' ).innerHTML = totalPrice;
                            }

                        } else { alert( 'Input quantity of item' ); }
                    } else { alert( 'Input price of item' ); }
                } else { alert( 'Input name of item' ); }
                
                // createTotalRow = basketList.insertRow(-1);
                // createTotalRow.setAttribute( 'class', 'total-row' );
                // createTotalRowLabel = createTotalRow.insertCell(0);
                // createTotalRowLabel.innerHTML = 'Total Price';
                // createTotalRowLabel.colSpan = '4';
                // createTotalRowPrice = createTotalRow.insertCell(-1);
                // createTotalRowPrice.innerHTML = totalPrice; // created repeatedly
            } );
        </script>
    </div>


    <!-- EXTERNAL SCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>